#include <stdio.h>
#include <stdlib.h>

struct node
{
	int data;
	struct node *nextnode;
}*temp=NULL;

struct hash
{
	int value;
	struct node *next;
} ;

int main()
{
	int u,v,edges,vertices,i,j;
	scanf("%d%d",&edges,&vertices);
	struct hash *hashtable;
	hashtable= (struct hash *) calloc (vertices , sizeof(struct hash));
	printf("***vert %d****edge %d****\n",vertices,edges);
	for(i=0;i<vertices;i++)
	{
		hashtable[i].next=NULL;
	}

	for(i=0;i<edges;i++)
	{
		scanf("%d%d",&u,&v);
		struct node *newnode;
		newnode= (struct node *) malloc (sizeof(struct node));
		newnode->data=u;
		newnode->nextnode=NULL;
		if(hashtable[v].next==NULL)
		{
			hashtable[v].next=newnode;
		}
		else
		{
			newnode->nextnode=hashtable[v].next;
			hashtable[v].next=newnode;
		}
	}

	for(i=0;i<vertices;i++)
	{
		temp=hashtable[i].next;
		printf("%d ------- ",i+1);
		while(temp!=NULL)
		{
			printf("%d --> ",temp->data);
			temp=temp->nextnode;
		}
		printf("NULL\n");
	}
	return 0;
}